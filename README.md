<h1>Devoir UQAC - AjChess</h1>

<p><h2>Description du travail</h2>
Devoir ayant pour but d'intégrer les règles de déplacements des pièces d'un jeu d'échecs
à partir d'un code fourni sans le modifier directement, mais en ajoutant des aspects.
</p>

<p><h2>Technologies et langages utilisés</h2>
<ul><li>Eclipse
    <li>Java
    <li>AspectJ
</ul></p>

<p><h2>Participants</h2>
<ul><li>Ludovic Jozereau
</ul></p>

<p><h3>Pas d'exécutable disponible</h3></p>